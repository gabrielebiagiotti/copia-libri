﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopiaLibri
{
    class Program
    {
        static void Main(string[] args)
        {
            string basePath = @"C:\Users\gabri\Desktop\ebooks\Ebook Emule Collection - Luglio 2009 (Ita Libro)\(Ita Libro) K...Z";
            var fileNames = Directory.GetFiles(basePath);
            var authors = new Dictionary<string, DirectoryInfo>();
            var sistematiDir = Directory.CreateDirectory(basePath + "\\sistemati");
            foreach (var fileName in fileNames)
            {
                try
                {

                    var name = Path.GetFileName(fileName);
                    if (name.Contains("-"))
                    {
                        var author = name.Substring(0, name.IndexOf('-')).Trim();
                        DirectoryInfo authorDir = null;
                        if (!Directory.Exists(basePath + "\\" + author))
                            authorDir = Directory.CreateDirectory(basePath + "\\" + author);
                        else
                            authorDir = new DirectoryInfo(basePath + "\\" + author);

                        File.Copy(fileName, authorDir.FullName + "\\" + name, true);
                        File.Move(fileName, sistematiDir.FullName + "\\" + name);
                        File.WriteAllLines(sistematiDir.FullName + @"\resoconto.txt", new string[] { fileName });
                        Console.WriteLine(fileName);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
               
            }
        }
    }
}
